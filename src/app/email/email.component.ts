import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit {
  pattern: RegExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/


  constructor() { }

  ngOnInit(): void {
  }

  validityEmail(email: string): any {
    if(email.match(this.pattern)) {
      return true
    }
    return false
  }

}
